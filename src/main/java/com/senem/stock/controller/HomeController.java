package com.senem.stock.controller;

import com.senem.stock.model.Product;
import com.senem.stock.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/app")
@Slf4j
public class HomeController {

    private ProductRepository productRepository;

    @Autowired
    public HomeController(ProductRepository productRepository){
        this.productRepository = productRepository;
        this.productRepository.save(new Product("Pencil",10.00));
        this.productRepository.save(new Product("Bag",48.00));
    }
    @GetMapping
     public String homeController(Model model){
          model.addAttribute("products",this.productRepository.findAll());
         return "app";
}

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String handleAddUser(Product product) {
        this.productRepository.save(product);
        return "redirect:/app";
    }


    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String Delete_Handle(@RequestParam(name = "productId") Long productId){
         productRepository.deleteById(productId);
         return "redirect:/app";
    }


}
