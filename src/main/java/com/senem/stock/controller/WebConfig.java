package com.senem.stock.controller;

import org.springframework.context.annotation.Configuration;

import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
     public  void  addViewControllers(ViewControllerRegistry registry ){
        registry.addViewController("/login");
        registry.addViewController("/logout");
        registry.addViewController("/").setViewName("login");
        registry.addViewController("/register").setViewName("register");
    }
}
