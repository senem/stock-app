package com.senem.stock.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import java.util.Date;

@NoArgsConstructor
@Entity
@Data
public class Product {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Double price;
    private Date createdAt;


    public  Product(String name,Double price){
            super();
            this.name=name;
            this.price=price;

    }
    @PrePersist
    void CreatedAt(){
        createdAt = new Date();
    }

}
