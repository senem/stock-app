package com.senem.stock.model;

import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;

@Data
public class RegisterForm {
    private String username;
    private String password;
    private String email;
    private String phoneNumber;

    public User  toUser(PasswordEncoder passwordEncoder) {
        return new User (
                username,passwordEncoder.encode(password),
                email,phoneNumber);
    }

}
