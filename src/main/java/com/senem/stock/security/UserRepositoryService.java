package com.senem.stock.security;

import com.senem.stock.model.User;
import com.senem.stock.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service

public class UserRepositoryService implements UserDetailsService {
     private UserRepository userRepository;

     @Autowired
     public  UserRepositoryService(UserRepository userRepository){
         this.userRepository=userRepository;


     }

     @Override
    public UserDetails loadUserByUsername(String username)
         throws UsernameNotFoundException{
         User user = userRepository.findByUsername(username);
         if (user != null){
             return  user;
         }
         throw  new UsernameNotFoundException("User'"+username+"'not found");

     }

}

