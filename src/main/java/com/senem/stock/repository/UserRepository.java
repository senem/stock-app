package com.senem.stock.repository;

import com.senem.stock.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository < User,Long> {
     User findByUsername(String username);

}
